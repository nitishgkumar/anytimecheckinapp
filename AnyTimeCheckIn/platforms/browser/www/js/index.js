var searchBoxInput;
var checkInDateInput;
var checkOutDateInput;
var timePickerDiv;
var checkInTimeInput;
var checkOutTimeInput;
var filterDiv;
var roomHomeCount;
var adultHomeCount;
var childHomeCount;
var searchButton;

// filterButton var

var adultCountPlusButton;
var adultCountMinusButton;
var childCountPlusButton;
var childCountMinusButton;
var roomFilterPlusButton;
var roomFilterMinusButton;
var adultFilterCountText;
var childtFilterCountText;
var roomFilterCountText;
var filterSubmitButton;
var filterBackButton;


var mapPinButton;
var mapPinDoneButton;

var map;
var snackBarText;


var searchPage = "searchPage";
var filterPage = "filterPage";
var mapPage = "map-page";
var currentPage;




function onLoad() {
    document.addEventListener("deviceready", deviceReady, false);
}


/*
 onclick='setGoogleSearch()'
 onchange="dateChange()"
 onclick="searchButtonClick()"
*/

function deviceReady() {
    console.log("deviceReady");
    // Make the request
    //    navigator.geolocation.getCurrentPosition(onSuccess, onError);            
    init();
    setHomeScreen();
    setGoogleSearch();
    setUpListeners();
    // checkConnection();
    requestLocationAccuracy();
    getUserLocaion();

}

function showSnakBar(msg) {
    //    cordova.plugins.snackbar.create('This is a indefinite snackbar text', 'INDEFINITE', "Dismiss", function(){
    //      console.log('Dismiss Button Clicked!');
    //    });
    //    cordova.plugins.snackbar.close(function(){
    //      console.log('Snackbar Closed Programmatically!');
    //    });

    //  window.plugins.toast.showShortTop("HEllo");

    snackBarText.innerHTML = "" + msg;
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

function setHomeScreen() {
    //   var todayDate=new Date();
    //   
    //    console.log("setHomeScreen = todayDate "+todayDate);
    //    
    //    checkInDateInput.setDate=new Date();
    //    checkOutDateInput.setDate="12-08-2010"

    //TODO Set the cuurent Date 


//    checkInDateInput.value = window.localStorage.getItem(checkInDateValueKey);
//    checkInTimeInput.value = window.localStorage.getItem(checkInTimeValueKay);
//    checkOutDateInput.value = window.localStorage.getItem(checkOutDateValueKey);
//    checkOutTimeInput.value = window.localStorage.getItem(checkOutTimeValueKey);
}


function setUpListeners() {

    document.addEventListener("backbutton", onBackButton);
    filterDiv.addEventListener("click", showFilter);
    checkInDateInput.addEventListener("change", checkInDateChange);
    checkOutDateInput.addEventListener("change", checkOutDateChange);
    //filterDiv.addEventListener("onclick",showFilter,false);
    searchButton.addEventListener("click", search);
    adultCountPlusButton.addEventListener("click", adultPlus);
    adultCountMinusButton.addEventListener("click", adultMinus);
    childCountPlusButton.addEventListener("click", childPlus);
    childCountMinusButton.addEventListener("click", childMinus);
    roomFilterPlusButton.addEventListener("click", roomPlus);
    roomFilterMinusButton.addEventListener("click", roomMinus);
    filterBackButton.addEventListener("click", onBackButton);
    filterSubmitButton.addEventListener("click", filterSubmit);
    mapPinButton.addEventListener("click", openMapLocationChoosers);
    mapPinDoneButton.addEventListener("click", mapPinDone);

    searchBoxInput.addEventListener("click", clearInput);
    // filterButton var



}

function clearInput() {
    searchBoxInput.value = "";

}

function mapPinDone() {
    openSearchPage();
}

function openMapLocationChoosers() {
    openMapPage();

}

function showFilter() {
    console.log("showFilter");
    openFilterPage();
}

function filterSubmit() {
    adultHomeCount.innerHTML = adultFilterCountText.innerHTML;
    childHomeCount.innerHTML = childtFilterCountText.innerHTML;
    roomHomeCount.innerHTML = roomFilterCountText.innerHTML;
    openSearchPage();
}

function onBackButton() {
    openSearchPage();
    //    navigator.app.exitApp();

}

function init() {

    searchBoxInput = document.getElementById("searchBoxInput");
    checkInDateInput = document.getElementById("checkInDateInput");
    checkOutDateInput = document.getElementById("checkOutDateInput");
    timePickerDiv = document.getElementById("timePickerDiv");
    checkInTimeInput = document.getElementById("checkInTimeInput");
    checkOutTimeInput = document.getElementById("checkOutTimeInput");
    filterDiv = document.getElementById("filterDiv");
    roomHomeCount = document.getElementById("roomHomeCount");
    adultHomeCount = document.getElementById("adultHomeCount");
    childHomeCount = document.getElementById("childHomeCount");
    searchButton = document.getElementById("searchButton");
    snackBarText = document.getElementById("snackBarText");
    adultCountPlusButton = document.getElementById("adultCountPlusButton");
    adultCountMinusButton = document.getElementById("adultCountMinusButton");
    childCountPlusButton = document.getElementById("childCountPlusButton");
    childCountMinusButton = document.getElementById("childCountMinusButton");
    roomFilterPlusButton = document.getElementById("roomFilterPlusButton");
    roomFilterMinusButton = document.getElementById("roomFilterMinusButton");
    adultFilterCountText = document.getElementById("adultFilterCountText");
    childtFilterCountText = document.getElementById("childtFilterCountText");
    roomFilterCountText = document.getElementById("roomFilterCountText");
    filterBackButton = document.getElementById("filterBackButton");
    filterSubmitButton = document.getElementById("filterSubmitButton");
    mapPinButton = document.getElementById("mapPinButton");
    mapPinDoneButton = document.getElementById("mapPinDoneButton");
}


function checkInDateChange() {
    console.log("checkInTimeChange");
    // alert("checkInTimeChange");
    //  timePickerDiv["display-origin"]('display', 'inline-flex');
    var checkInDate = new Date(checkInDateInput.value);
    var checkOutDate = new Date(checkOutDateInput.value);


    if (checkInDate > checkOutDate) {
        showSnakBar("invalid date");
    }


    showTimeDiv(checkInDate, checkOutDate);

}

function checkOutDateChange() {
    console.log("checkOutTimeChange");
    var checkInDate = new Date(checkInDateInput.value);
    var checkOutDate = new Date(checkOutDateInput.value);
    if (checkInDate > checkOutDate) {
        showSnakBar("invalid date");
    }

    showTimeDiv(checkInDate, checkOutDate);
}


function showTimeDiv(checkInDate, checkOutDate) {
    console.log("showTimeDiv");

    var checkInDay = checkInDate.getDate();
    var checkInMonth = checkInDate.getMonth();
    var checkInYear = checkInDate.getFullYear();
    console.log("day = " + checkInDay + " month = " + checkInMonth + "year = " + checkInYear);


    var checkOutDay = checkOutDate.getDate();
    var checkOutMonth = checkOutDate.getMonth();
    var checkOutYear = checkOutDate.getFullYear();
    console.log("day = " + checkOutDay + " month = " + checkOutMonth + "year = " + checkOutYear);

    if (checkInYear > 0 && checkOutYear > 0) {
        if (checkOutYear == checkInYear && checkInMonth == checkOutMonth && checkInDay == checkOutDay) {
            $('#timePickerDiv').css('display', 'inline-flex');
        } else {
            $('#timePickerDiv').css('display', 'none');

        }
    }



}

function dateChange() {

    var date = new Date($('#checkInTimeInput').val());
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();

    //$("#bookingTimePickerDiv").css=  
    $('#timePickerDiv').css('display', 'inline-flex');
    //alert([day, month, year].join('/'));
    //console.log("dateChange()");
    //alert($("#checkInTimeInputs").val()); 

}

function getUserLocaion() {

    navigator.geolocation.getCurrentPosition(userLocationSuccess, userLocationError);
    //    navigator.geolocation.activator.askActivation(function (response) {    
    //    }, function (response) {
    //        //Failure callback
    //    });


    drawMap(34.0983425, -118.3267434);
}

function userLocationSuccess(position) {

    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'latLng': latlng
    }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {

                // document.getElementById("googleSearchBox").value=results[1].formatted_address;
                //  alert("Location: " + results[1].formatted_address);
                searchBoxInput.value = "near me";

                // 34.0983425, -118.3267434
                drawMap(position.coords.latitude, position.coords.longitude);
                //drawMap(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            }
        }
    });
}

function userLocationError(error) {
    alert('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

function setGoogleSearch() {
    // var searchBox = new google.maps.places.SearchBox(document.getElementById('fname'));



    var options = {
        componentRestrictions: {
            country: 'in'
        }
    };
    //TODO change the country name in the type

    var searchBox = new google.maps.places.SearchBox(searchBoxInput, options);
    // [START region_getplaces]
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }
    });

}

function search() {
    window.location = "searchResult.html";

    //openMapPage();
}

function openSearchPage() {
    window.location.href = "#" + searchPage;
    currentPage = searchPage;

}

function openFilterPage() {
    window.location.href = "#" + filterPage;
    currentPage = filterPage;
}

function openMapPage() {
    window.location.href = "#" + mapPage;
    currentPage = mapPage;
}



function childPlus() {

    var count = childtFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer <= 6) {

        integer = integer + 1;
        childtFilterCountText.innerHTML = integer;
    }


}


function childMinus() {

    var count = childtFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer > 0) {
        integer = integer - 1;
        childtFilterCountText.innerHTML = integer;
    }
}




function adultPlus() {


    var count = adultFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer <= 6) {
        integer = integer + 1;
        adultFilterCountText.innerHTML = integer;
    }
}

function adultMinus() {

    var count = adultFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer > 0) {
        integer = integer - 1;
        adultFilterCountText.innerHTML = integer;
    }
}

function roomPlus() {

    var count = roomFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer <= 6) {
        integer = integer + 1;
        roomFilterCountText.innerHTML = integer;
    }
}

function roomMinus() {

    var count = roomFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer > 0) {
        integer = integer - 1;
        roomFilterCountText.innerHTML = integer;
    }
}


function drawMap(lat, lng) {

    //    var mapOptions = {
    //        credentials: "AnrjhfHfdwa6ZQf_H628baJvQMBbJ2YQVrqzQyX4jLuzTIy-1eafF1ywsFri-0l2",
    //        mapTypeId: Microsoft.Maps.MapTypeId.road,
    //        center: new Microsoft.Maps.Location(43.069452, -89.411373),
    //        zoom: 11
    //    };
    //    var map = new Microsoft.Maps.Map(document.getElementById("map-canvas"), mapOptions);


    //    $('#map-canvas').locationpicker({
    //        location: {
    //            latitude: 46.15242437752303,
    //            longitude: 2.7470703125
    //        },
    //        radius: 300,
    //        inputBinding: {
    //            //latitudeInput: $('#us3-lat'),
    //            //longitudeInput: $('#us3-lon'),
    //            //radiusInput: $('#us3-radius'),
    //            //locationNameInput: $('#us3-address')
    //        },
    //        enableAutocomplete: true,
    //        onchanged: function (currentLocation, radius, isMarkerDropped) {
    //            // Uncomment line below to show alert on each Location Changed event
    //            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    //        }
    //    });

    //    var myOptions = {
    //        zoom: 10,
    //        center: latlng,
    //        mapTypeId: google.maps.MapTypeId.ROADMAP
    //    };
    //    var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    //    // Add an overlay to the map of current lat/lng
    //    var marker = new google.maps.Marker({
    //        position: latlng,
    //        map: map,
    //        title: "Greetings!"
    //    });



    map = new Microsoft.Maps.Map('#map-canvas', {});

    //Add a standard red pushpin that doesn't have dragging enabled.

    //Add a green pushpin that has dragging enabled and events attached to it.
    var greenPin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(lat, lng), {
        icon: 'https://www.bingmapsportal.com/Content/images/poi_custom.png',
        draggable: true
    });

    map.entities.push(greenPin);
    Microsoft.Maps.Events.addHandler(greenPin, 'drag', function (e) {
        highlight('pushpinDrag', e);
    });
    Microsoft.Maps.Events.addHandler(greenPin, 'dragend', function (e) {
        highlight('pushpinDragEnd', e);
    });
    Microsoft.Maps.Events.addHandler(greenPin, 'dragstart', function (e) {
        highlight('pushpinDragStart', e);
    });

}

function GetMap() {
    map = new Microsoft.Maps.Map('#map-canvas', {});
    var center = map.getCenter();
    //Add a standard red pushpin that doesn't have dragging enabled.

    //Add a green pushpin that has dragging enabled and events attached to it.
    var greenPin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(center.latitude, center.longitude), {
        icon: 'https://www.bingmapsportal.com/Content/images/poi_custom.png',
        draggable: true
    });

    map.entities.push(greenPin);
    Microsoft.Maps.Events.addHandler(greenPin, 'drag', function (e) {
        highlight('pushpinDrag', e);
    });
    Microsoft.Maps.Events.addHandler(greenPin, 'dragend', function (e) {
        highlight('pushpinDragEnd', e);
    });
    Microsoft.Maps.Events.addHandler(greenPin, 'dragstart', function (e) {
        highlight('pushpinDragStart', e);
    });




}

function highlight(id, event) {
    searchBoxInput.value = "Pin on map";
    console.log(" event.target.getLocation() latitude " + event.target.getLocation().latitude);
    console.log("event.target.getLocation() longitude" + event.target.getLocation().longitude);
    //Highlight the mouse event div to indicate that the event has fired.
    // document.getElementById(id).style.background = 'LightGreen';
    //document.getElementById('pushpinLocation').innerText = event.target.getLocation().toString();
    //Remove the highlighting after a second.

}

function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.CELL] = 'Cell generic connection';
    states[Connection.NONE] = 'No network connection';

    alert('Connection type: ' + states[networkState]);
}

function onLocationRequestError(error) {
    console.error("The following error occurred: " + error);
}

function handleLocationAuthorizationStatus(cb, status) {
    switch (status) {
        case cordova.plugins.diagnostic.permissionStatus.GRANTED:
            cb(true);
            break;
        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
            requestLocationAuthorization(cb);
            break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED:
            cb(false);
            break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
            // Android only
            cb(false);
            break;
        case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
            // iOS only
            cb(true);
            break;
    }
}

function requestLocationAuthorization(cb) {
    cordova.plugins.diagnostic.requestLocationAuthorization(handleLocationAuthorizationStatus.bind(this, cb), onLocationRequestError);
}

function ensureLocationAuthorization(cb) {
    cordova.plugins.diagnostic.getLocationAuthorizationStatus(handleLocationAuthorizationStatus.bind(this, cb), onLocationRequestError);
}

function requestLocationAccuracy() {
    ensureLocationAuthorization(function (isAuthorized) {
        if (isAuthorized) {
            cordova.plugins.locationAccuracy.canRequest(function (canRequest) {
                if (canRequest) {
                    cordova.plugins.locationAccuracy.request(function () {
                            console.log("Request successful");
                        }, function (error) {
                            onError("Error requesting location accuracy: " + JSON.stringify(error));
                            if (error) {
                                // Android only
                                onLocationRequestError("error code=" + error.code + "; error message=" + error.message);
                                if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {
                                    if (window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")) {
                                        cordova.plugins.diagnostic.switchToLocationSettings();
                                    }
                                }
                            }
                        }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY // iOS will ignore this
                    );
                } else {
                    // On iOS, this will occur if Location Services is currently on OR a request is currently in progress.
                    // On Android, this will occur if the app doesn't have authorization to use location.
                    onLocationRequestError("Cannot request location accuracy");
                }
            });
        } else {
            onLocationRequestError("User denied permission to use location");
        }
    });
}
