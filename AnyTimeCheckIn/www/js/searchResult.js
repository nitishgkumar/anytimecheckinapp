var mapBackButton;
var mapSwitchButton;
var mapCheckInDateInput;
var mapCheckOutDateInput;
var mapCheckInTimeInput;
var mapCheckOutTimeInput;
var mapFilterButton;
var mapFilterAddressTextView;
var mapPage = "mapPage";
var map;
var mapCanvas;

var listBackButton;
var listSwitchButton;
var listCheckInDateInput;
var listCheckOutDateInput;
var listCheckInTimeInput;
var listCheckOutTimeInput;
var listFilterButton;
var listFilterAddressTextView;

var hotelListPage = "hotelListPage";
var currentPage = "";

var hotelListView;



// filterButton var

var adultCountPlusButton;
var adultCountMinusButton;
var childCountPlusButton;
var childCountMinusButton;
var roomFilterPlusButton;
var roomFilterMinusButton;
var adultFilterCountText;
var childtFilterCountText;
var roomFilterCountText;
var filterSubmitButton;
var filterBackButton;



function onLoad() {
    document.addEventListener("deviceready", deviceReady, false);
}

function init() {

    mapBackButton = document.getElementById("mapBackButton");
    mapSwitchButton = document.getElementById("mapSwitchButton");
    mapCheckInDateInput = document.getElementById("mapCheckInDateInput");
    mapCheckOutDateInput = document.getElementById("mapCheckOutDateInput");
    mapCheckInTimeInput = document.getElementById("mapCheckInTimeInput");
    mapCheckOutTimeInput = document.getElementById("mapCheckOutTimeInput");
    mapFilterAddressTextView = document.getElementById("mapFilterAddressTextView");
    mapFilterAddressTextView = document.getElementById("mapFilterAddressTextView");
    mapFilterButton = document.getElementById("mapFilterButton");
    mapCanvas = document.getElementById("mapCanvas");
    listBackButton = document.getElementById("listBackButton");
    listSwitchButton = document.getElementById("listSwitchButton");
    listCheckInDateInput = document.getElementById("listCheckInDateInput");
    listCheckOutDateInput = document.getElementById("listCheckOutDateInput");
    listCheckInTimeInput = document.getElementById("listCheckInTimeInput");
    listCheckOutTimeInput = document.getElementById("listCheckOutTimeInput");
    listFilterButton = document.getElementById("listFilterButton");
    listFilterAddressTextView = document.getElementById("listFilterAddressTextView");
    hotelListView = document.getElementById("hotelListView");
    hotelListPage = document.getElementById("hotelListPage");

}




function setUpListeners() {

    document.addEventListener("backbutton", onBackButton);
    mapBackButton.addEventListener("click", onBackButton);
    mapSwitchButton.addEventListener("click", mapSwitchButtonClick);
    mapFilterButton.addEventListener("click", mapFilterButtonClick);

    mapCheckInDateInput.addEventListener("change", mapCheckInDateInputChange);
    mapCheckOutDateInput.addEventListener("change", mapCheckOutDateInputChange);
    mapCheckInTimeInput.addEventListener("change", mapCheckInTimeInputChange);
    mapCheckOutTimeInput.addEventListener("change", mapCheckOutTimeInputChange);

    //filterDiv.addEventListener("onclick",showFilter,false);
    listBackButton.addEventListener("click", onBackButton);
    listSwitchButton.addEventListener("click", listSwitchButtonClick);


    listCheckInDateInput.addEventListener("change", listCheckInDateInputChange);
    listCheckOutDateInput.addEventListener("change", listCheckOutDateInputChange);
    listCheckInTimeInput.addEventListener("change", listCheckInTimeInputChange);
    listCheckOutTimeInput.addEventListener("change", listCheckOutTimeInputChange);

    listFilterButton.addEventListener("click", listFilterButtonClick);
}

function setHomeScreen() {
    
    
    
//    mapCheckInDateInput.value = new Date();
//    mapCheckOutDateInput.value = new Date();
//    mapCheckInTimeInput.value = new Date();
//    mapCheckOutTimeInput.value = new Date();
//
//    listCheckInDateInput.value = new Date();
//    listCheckOutDateInput.value = new Date();
//    listCheckInTimeInput.value = new Date();
//    listCheckOutTimeInput.value = new Date();

}







function mapSwitchButtonClick() {

}

function mapFilterButtonClick() {

}

function mapCheckInDateInputChange() {
    console.log("mapCheckInDateInputChange");

    listCheckInDateInput.value = mapCheckInDateInput.value;
}

function mapCheckOutDateInputChange() {
    console.log("mapCheckOutDateInputChange");
    listCheckOutDateInput.value = mapCheckOutDateInput.value;

}

function mapCheckInTimeInputChange() {
    console.log("mapCheckInTimeInputChange");
    listCheckInTimeInput.value = mapCheckInTimeInput.value;
}

function mapCheckOutTimeInputChange() {
    console.log("mapCheckOutTimeInputChange");
    listCheckOutTimeInput.value = mapCheckOutTimeInput.value;
}

function listSwitchButtonClick() {

}

function listCheckInDateInputChange() {
    console.log("listCheckInDateInputChange");

    mapCheckInDateInput.value = listCheckInDateInput.value;
}


function listCheckOutDateInputChange() {
     console.log("listCheckOutDateInputChange");
    mapCheckOutDateInput.value = listCheckOutDateInput.value;
}

function listCheckInTimeInputChange() {
     console.log("listCheckInTimeInputChange");
    mapCheckInTimeInput.value = listCheckInTimeInput.value;
}

function listCheckOutTimeInputChange() {
    console.log("listCheckOutTimeInput");
    mapCheckOutTimeInput.value = listCheckOutTimeInput.value;
}

function listFilterButtonClick() {
    openFilterButton();

}

function openFilterButton() {
 //   window.location.href = "#" + "filterPage";
}

function checkInDateChange() {
    console.log("checkInTimeChange");
    // alert("checkInTimeChange");
    //  timePickerDiv["display-origin"]('display', 'inline-flex');
    var checkInDate = new Date(checkInDateInput.value);
    var checkOutDate = new Date(checkOutDateInput.value);


    if (checkInDate > checkOutDate) {
        showSnakBar("invalid date");
    }


    showTimeDiv(checkInDate, checkOutDate);

}

function checkOutDateChange() {
    console.log("checkOutTimeChange");
    var checkInDate = new Date(checkInDateInput.value);
    var checkOutDate = new Date(checkOutDateInput.value);
    if (checkInDate > checkOutDate) {
        showSnakBar("invalid date");
    }

    showTimeDiv(checkInDate, checkOutDate);
}


function showTimeDiv(checkInDate, checkOutDate) {
    console.log("showTimeDiv");

    var checkInDay = checkInDate.getDate();
    var checkInMonth = checkInDate.getMonth();
    var checkInYear = checkInDate.getFullYear();
    console.log("day = " + checkInDay + " month = " + checkInMonth + "year = " + checkInYear);


    var checkOutDay = checkOutDate.getDate();
    var checkOutMonth = checkOutDate.getMonth();
    var checkOutYear = checkOutDate.getFullYear();
    console.log("day = " + checkOutDay + " month = " + checkOutMonth + "year = " + checkOutYear);

    if (checkInYear > 0 && checkOutYear > 0) {
        if (checkOutYear == checkInYear && checkInMonth == checkOutMonth && checkInDay == checkOutDay) {
            $('#timePickerDiv').css('display', 'inline-flex');
        } else {
            $('#timePickerDiv').css('display', 'none');

        }
    }



}

function dateChange() {

    var date = new Date($('#checkInTimeInput').val());
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();

    //$("#bookingTimePickerDiv").css=  
    $('#timePickerDiv').css('display', 'inline-flex');
    //alert([day, month, year].join('/'));
    //console.log("dateChange()");
    //alert($("#checkInTimeInputs").val()); 

}

function deviceReady() {
    console.log("deviceReady");
    init();
    setUpListeners();
    setHomeScreen();

    searchResult();
    drawMap(2, 5);


    $("#filterPage").load("filterPage.html", filterSuccessful);
}

function childPlus() {

    var count = childtFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer <= 6) {

        integer = integer + 1;
        childtFilterCountText.innerHTML = integer;
    }


}


function childMinus() {

    var count = childtFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer > 0) {
        integer = integer - 1;
        childtFilterCountText.innerHTML = integer;
    }
}




function adultPlus() {


    var count = adultFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer <= 6) {
        integer = integer + 1;
        adultFilterCountText.innerHTML = integer;
    }
}

function adultMinus() {

    var count = adultFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer > 0) {
        integer = integer - 1;
        adultFilterCountText.innerHTML = integer;
    }
}

function roomPlus() {

    var count = roomFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer <= 6) {
        integer = integer + 1;
        roomFilterCountText.innerHTML = integer;
    }
}

function roomMinus() {

    var count = roomFilterCountText.innerHTML;

    var integer = parseInt(count, 10);
    if (integer > 0) {
        integer = integer - 1;
        roomFilterCountText.innerHTML = integer;
    }
}

function filterSuccessful() {
    alert("filterSuccessful");

    adultCountPlusButton = document.getElementById("adultCountPlusButton");
    adultCountMinusButton = document.getElementById("adultCountMinusButton");
    childCountPlusButton = document.getElementById("childCountPlusButton");
    childCountMinusButton = document.getElementById("childCountMinusButton");
    roomFilterPlusButton = document.getElementById("roomFilterPlusButton");
    roomFilterMinusButton = document.getElementById("roomFilterMinusButton");
    adultFilterCountText = document.getElementById("adultFilterCountText");
    childtFilterCountText = document.getElementById("childtFilterCountText");
    roomFilterCountText = document.getElementById("roomFilterCountText");



    adultCountPlusButton.addEventListener("click", adultPlus);
    adultCountMinusButton.addEventListener("click", adultMinus);
    childCountPlusButton.addEventListener("click", childPlus);
    childCountMinusButton.addEventListener("click", childMinus);
    roomFilterPlusButton.addEventListener("click", roomPlus);
    roomFilterMinusButton.addEventListener("click", roomMinus);
    filterBackButton.addEventListener("click", onBackButton);
    filterSubmitButton.addEventListener("click", filterSubmit);
}

function setUpFilterOptions() {

}

function openHotalListPage() {
    window.location.href = "#" + "map-page";

}

function openMapPage() {

}

function listSwitchButton() {

}

function mapSwitchButton() {

}

function openFiltter() {

}

function onBackButton() {

    openHomePage();

}

function openHomePage() {
    window.location = "index.html";
}




function drawMap(lat, lng) {

    //    var mapOptions = {
    //        credentials: "AnrjhfHfdwa6ZQf_H628baJvQMBbJ2YQVrqzQyX4jLuzTIy-1eafF1ywsFri-0l2",
    //        mapTypeId: Microsoft.Maps.MapTypeId.road,
    //        center: new Microsoft.Maps.Location(43.069452, -89.411373),
    //        zoom: 11
    //    };
    //    var map = new Microsoft.Maps.Map(document.getElementById("map-canvas"), mapOptions);


    //    $('#map-canvas').locationpicker({
    //        location: {
    //            latitude: 46.15242437752303,
    //            longitude: 2.7470703125
    //        },
    //        radius: 300,
    //        inputBinding: {
    //            //latitudeInput: $('#us3-lat'),
    //            //longitudeInput: $('#us3-lon'),
    //            //radiusInput: $('#us3-radius'),
    //            //locationNameInput: $('#us3-address')
    //        },
    //        enableAutocomplete: true,
    //        onchanged: function (currentLocation, radius, isMarkerDropped) {
    //            // Uncomment line below to show alert on each Location Changed event
    //            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
    //        }
    //    });

    //    var myOptions = {
    //        zoom: 10,
    //        center: latlng,
    //        mapTypeId: google.maps.MapTypeId.ROADMAP
    //    };
    //    var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    //    // Add an overlay to the map of current lat/lng
    //    var marker = new google.maps.Marker({
    //        position: latlng,
    //        map: map,
    //        title: "Greetings!"
    //    });


    console.log("draw Map");

    var map = new Microsoft.Maps.Map(document.getElementById('mapCanvas'), {
        credentials: 'YOURBINGMAPSKEY'
    });
    var pushpin = new Microsoft.Maps.Pushpin(map.getCenter(), null);
    map.entities.push(pushpin);


    //Add a standard red pushpin that doesn't have dragging enabled.

    //Add a green pushpin that has dragging enabled and events attached to it.
    //    var greenPin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(43.069452, -89.411373), {
    //        icon: 'https://www.bingmapsportal.com/Content/images/poi_custom.png',
    //        draggable: false
    //    });
    //
    ////    map.entities.push(greenPin);
    ////    Microsoft.Maps.Events.addHandler(greenPin, 'drag', function (e) {
    ////        highlight('pushpinDrag', e);
    ////    });
    ////    Microsoft.Maps.Events.addHandler(greenPin, 'dragend', function (e) {
    ////        highlight('pushpinDragEnd', e);
    ////    });
    ////    Microsoft.Maps.Events.addHandler(greenPin, 'dragstart', function (e) {
    ////        highlight('pushpinDragStart', e);
    ////    });

}

function GetMap() {

    var map = new Microsoft.Maps.Map(document.getElementById('mapCanvas'), {
        credentials: 'YOURBINGMAPSKEY'
    });
    var pushpin = new Microsoft.Maps.Pushpin(map.getCenter(), null);
    map.entities.push(pushpin);


    //    map = new Microsoft.Maps.Map('#map-canvas', {});
    //    var center = map.getCenter();
    //    //Add a standard red pushpin that doesn't have dragging enabled.
    //
    //    //Add a green pushpin that has dragging enabled and events attached to it.
    //    var greenPin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(center.latitude, center.longitude), {
    //        icon: 'https://www.bingmapsportal.com/Content/images/poi_custom.png',
    //        draggable: true
    //    });
    //
    //    map.entities.push(greenPin);
    //    Microsoft.Maps.Events.addHandler(greenPin, 'drag', function (e) {
    //        highlight('pushpinDrag', e);
    //    });
    //    Microsoft.Maps.Events.addHandler(greenPin, 'dragend', function (e) {
    //        highlight('pushpinDragEnd', e);
    //    });
    //    Microsoft.Maps.Events.addHandler(greenPin, 'dragstart', function (e) {
    //        highlight('pushpinDragStart', e);
    //    });




}


function searchResult() {
    console.log("searchResult  =  emailId = ");
    console.log("staffLogin  =  password = ");
    console.log("staffLogin  =  coustomer = ");

    console.log("searchResult  =  postTo = ");

    var sendInfo = {
        appname: "Check-In App",
        appversion: "beta",
        action: "login",
        customer: "fkd",
        u: "hfdu",
        pwd: "5f4dcc3b5aa765d61d8327deb882cf99"
    };

    $.ajax({
        type: "POST",
        url: "http://netmaxims.in/projects/Rest-&-Go/api/search.php",
        dataType: "json",
        success: staffLoginSussess,
        data: sendInfo
    });


    //        $.post(postTo,({appname: "Check-In App", appversion: "beta", u: emailId,pwd :"5f4dcc3b5aa765d61d8327deb882cf99",customer:coustomer,action:"login"}),
    //        staffLoginSussess,'json');
    //openScanPage();

}

function staffLoginError() {
    console.log("staffLoginError  =  user_id = ");
}


function staffLoginSussess(data) {
    // {"error":"Employee not found."}  result  1
    // {"user_id":"13", "firstname": "david", "lastname": "morris", "webservicepass": ""} result 2  

    console.log("staffLoginSussess = " + data[0]["HotelList"][0]["hotel_id"]);


    if (data != "") {

        var length = data[0]["HotelList"].length;
        console.log("langth    =" + length);
        console.log("data = " + data);
        var i;
        hotelListView.innerHTML = "";

        for (i = 0; i < length; i++) {

            hotelListView.innerHTML = hotelListView.innerHTML + "<div class='list-item'><div class='search-image'><img src='image/a.jpg' style='width:100%;'><div class='text-block'><h4><del>" + "$" + data[0]["HotelList"][i]["price_per_hour"] + "</del></h4><h2>" + "$" + data[0]["HotelList"][i]["price_per_day"] + "<span>" + "10% off" + "</span></h2></div><div class='text-block1'><h3>" + "0 Km" + "</h3></div></div><h5 class='sear-star'>" + data[0]["HotelList"][i]["hotel_name"] + "<span> <i class='fa fa-star' aria-hidden='true'></i> <i class='fa fa-star' aria-hidden='true'></i></span></h5><div class='sear-cion'><i class='fa fa-map-marker' aria-hidden='true'></i><p>" + data[0]["HotelList"][i]["hotel_address"] + "</p></div></div>";

        }
        //         
        //data[0]["HotelList"][i]["price_per_hour"]
        //         +data[0]["HotelList"][i]["price_per_day"]+
        //    +data[0]["HotelList"][i]["hotel_name"] +
        //         data[0]["HotelList"][i]["hotel_address"]


        //        queueSelector.innerHTML = "";
        //        for (i = 0; i < length; i++) {
        //            queueSelector.innerHTML = queueSelector.innerHTML + "<option value='" + data[i]['queue_id'] + "'>" + data[i]['name'] + "</option>";
        //            console.log("data = " + data[i]['queue_id']);
        //            // alert(data[0]);
        //        }
        // do something
    } else {

        console.log("getPatientListSuccess Error");
        //alert("Failr");


        // couldn't connect
    }


    console.log("staffLoginSussess = " + data);


}
